<?php

/**
 * Things that should be avaliable for all of application.
 */
define('PLATFORM_START', microtime(true)); // For benchmarking

/**
 * Platform Path
 * 
 * Returns the directory path to the platform.
 * 
 * @return string
 */
function platform_path() {
    return realpath(__DIR__ . '/../platform');
}

/**
 * Root path.
 * 
 * Returns the root path to this application's install
 * 
 * @return string
 */
function root_path() {
    return realpath(__DIR__ . '/../');
}

// -------------------------------------------------------------------------------------------------

/**
 * Pull in our autoloaders.
 */

require platform_path() . '/bootstrap/autoload.php';

/**
 * Debugging
 */
$debug = new \Phalcon\Debug();
$debug->listen();

/**
 * Load Inversion of Control container.
 */
$ioc = require platform_path() . '/bootstrap/IoC.php';

/**
 *  Handle the request
 */
$application = new \Phalcon\Mvc\Application($ioc);
echo $application->handle()->getContent();
