<?php

$config = new Phalcon\Config();

$config['general'] = array(
    
    'baseurl' => '/platform/',
    
    'expireSessionMinutes' => 10,
    
    'maxLoginAttemptsCaptcha' => 5,
    
    'maxLoginAttemptsLock' => 25,
);

$config['database'] = array(
    
    'adapter' => 'mysql',
    
    'host' => '',
    
    'username' => '',
    
    'password' => '',
    
    'dbname' => '',
);

$config['repo.user'] = array(
    
    'username' => '',
    
    'password' => '',
);

$config['captcha'] = array(
    
    'privateKey'    => '',
    
    'publicKey'     => '',
);

return $config;