<?php

/**
 * Currently, this Access Control List (ACL) denys access to EVERYTHING
 * except what is designated as public.
 */

$access = new Phalcon\Acl\Adapter\Memory();

/**
 * You have to explictly make something private.
 * 
 * From experience, I've learned that it's less work to allow everything, then lock down, than it is
 * to lock everything down, then allow.  With the later, you'd have to grant access to public 
 * resources AND authenticated resources.
 */
$access->setDefaultAction(Phalcon\Acl::ALLOW);

/**
 * Platform roles
 */

$roles = array(
    'User', // Authenticated
    'Guest',
);

/**
 * Effected Resources.
 */

$protectedResources = array(
    'home' => array('index'),
);

// -------------------------------------------------------------------------------------------------

/**
 * Process roles and resources.
 */

// Roles
foreach ($roles as $role) {
    $access->addRole($role);
}

// Add Resources
foreach ($protectedResources as $resource => $actions) {
    $access->addResource(new Phalcon\Acl\Resource($resource), $actions);
}

// Block access to blocked resources
foreach ($protectedResources as $resource => $actions) {
    foreach ($actions as $action) {
        $access->deny('Guest', $resource, $action);
    }    
}

return $access;

