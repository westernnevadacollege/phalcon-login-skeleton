<?php

$router = new Phalcon\Mvc\Router();

$router->removeExtraSlashes(true);

// By default, Phalcon routing is done through controllers - this isn't the only routing logic in 
// the application.  See Phalcon documentation for more details.

$router->add('/', array(
    
    'controller' => 'login',
    
    'action' => 'index',
    
))->setName('login');

$router->add('/logout', array(
    
    'controller' => 'login',
    
    'action' => 'logout'
    
))->setName('logout');

return $router;