<?php

use Phalcon\Mvc\Model;

class Users extends Model 
{    
    public function initialize()
    {
        $this->hasOne('uid', 'PersonInfo', 'EMPLID');
    }
    
    // ---------------------------------------------------------------------------------------------
    
    public static function createModel($uid)
    {
        $user = new Users($uid);
        $user->uid = $uid;
        $user->loginAttempts = 0;
        $user->save();
        
        return $user;
    }
    
    // ---------------------------------------------------------------------------------------------
    
    public function getFirstName()
    {
        return $this->personInfo->FIRST_NAME;
    }
    
    // ---------------------------------------------------------------------------------------------
    
    public function getLastName()
    {
        return $this->personInfo->LAST_NAME;
    }
    
    // ---------------------------------------------------------------------------------------------
    
    public function getFullName()
    {
        return $this->personInfo->FIRST_NAME . ' ' . $this->personInfo->LAST_NAME;
    }
    
    // ---------------------------------------------------------------------------------------------
    
    public function incrementLoginAttempt()
    {
        $this->loginAttempts++;
        $this->save();
    }
    
    // ---------------------------------------------------------------------------------------------
    
    public function resetLoginAttempts()
    {
        $this->loginAttempts = 0;
        $this->save();
    }
    
    // ---------------------------------------------------------------------------------------------
    
    public function getLoginAttempts()
    {
        return $this->loginAttempts;
    }
}

//    /**
//     * Biographical class
//     *
//     * @var \WesternNevadaCollege\PersonInfo\Biographical 
//     */
//    protected $personInfo;
//    
//    // ---------------------------------------------------------------------------------------------
//    
//    public function initialize()
//    {
//        $di = $this->getDI();
//        $this->personInfo = $di->get('personInfo');
//    }
//    
//    // ---------------------------------------------------------------------------------------------
//    
//    public function getFirstName()
//    {
//        return $this->personInfo->getFirstName($this->uid);
//    }
//    
//    // ---------------------------------------------------------------------------------------------
//    
//    public function getLastName()
//    {
//        return $this->personInfo->getLastName($this->uid);
//    }
//    
//    // ---------------------------------------------------------------------------------------------
//    
//    public function getFullName()
//    {
//        return $this->personInfo->getFullName($this->uid);
//    }