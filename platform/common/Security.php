<?php

use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Acl;

use Carbon\Carbon;

class Security extends Plugin
{    
    /**
     * Activity object.
     * 
     * @var \Activity 
     */
    protected $activity;
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Constructor
     * 
     * Set's dependencies.
     */
    public function __construct()
    {
        $this->activity = new Activity();
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Do before route execution.
     * 
     * Allows the interjection of logic before framework components fire.
     * 
     * @param \Phalcon\Events\Event $event
     * @param \Phalcon\Mvc\Dispatcher $dispatcher
     * @return boolean
     */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {                
        if ($this->session->get('auth') !== null) {
            if ($this->sessionTimeout() === true) {
                return false;
            }
        }

        if ($this->checkAccess($dispatcher) === false) {
            return false;
        }
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Check access.
     * 
     * Checks user's access to components. Redirects if user does not have access.
     * 
     * @param \Phalcon\Mvc\Dispatcher $dispatcher
     * @return boolean
     */
    protected function checkAccess(Dispatcher $dispatcher)
    {
        $role = $this->getUserRole();
        
        // What's trying to be accessed.
        $dispatchedController = $dispatcher->getControllerName();
        $dispatchedAction = $dispatcher->getActionName();

        // Check if person has access.
        $access = $this->getAcl()->isAllowed($role, $dispatchedController, $dispatchedAction);
        
        // Boot out if they don't have access.
        if ($access != Acl::ALLOW) {
            
            $this->flash->error('Not authorized');
            
            $this->response->redirect(array(
                'for' => 'login'
            ));
            
            return false;
        }
        
        return true;
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Determines what role the user is.
     * 
     * Simple logic for now.
     * 
     * @return string
     */
    protected function getUserRole()
    {
        $session = $this->session->get('auth');
        
        return isset($session) ? 'User' : 'Guest';
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Get Access control list.
     * 
     * Fetches action control list from session.  Loads from config file if it hasn't been.
     * 
     * @return \Phalcon\Acl\Adapter\Memory
     */
    protected function getAcl()
    {
        if (!isset($this->persistent->acl)) {
            $this->persistent->acl = require platform_path() . '/_config/access.php';
        }
        
        return $this->persistent->acl;
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Check if session has expired.
     * 
     * Will read user's last navigation time and will log user out if the designated amount of
     * minutes in the platform's configuration has passed.
     * 
     * @return boolean
     */
    protected function sessionTimeout()
    {        
        if (!isset($this->persistent->expireMinutes)) {
            $config = require platform_path() . '/_config/platform.php';
            $this->persistent->expireMinutes = $config->general->expireSessionMinutes;
        }
        
        $lastActiviteTime = $this->activity->getLastNavigationTime();
        
        if ($lastActiviteTime <= Carbon::now()->subMinutes($this->persistent->expireMinutes)) {            
            $this->expireSession();
            return true;
        }
        
        $this->activity->setLastNavigationTime(Carbon::now());
        
        return false;
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Expires session.
     * 
     * @return void
     */
    protected function expireSession()
    {
        $this->flash->notice("You've been logged out for inactivity");
        
        // I'm initiating the login controller and exectuting its actions 
        // directly instead of a redirect due to a redirect loop that would 
        // occur. The fact that this class's logic depends on the current
        // session is what causes the redirect loop. The redirection would cause
        // this class's logic to never allow the dispatcher to reach the login
        // controller upon redirection. Thus, we call it ourselves.
        $logoutController = new LoginController();
        
        return $logoutController->logoutAction();
    }
}