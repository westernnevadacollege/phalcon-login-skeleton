<?php

use Phalcon\Mvc\User\Plugin;
use Carbon\Carbon;

class Activity extends Plugin 
{
    /**
     * Time of last navigation.
     * 
     * Returns the last time the user navigated.
     * 
     * @return \DateTime
     */
    public function getLastNavigationTime()
    {
        if (!isset($this->session->lastNavigation)) {
            $this->session->set('lastNavigation', Carbon::now());
        }
        
        return $this->session->lastNavigation;
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Set time of last navigation.
     * 
     * Set's the last time the user navigated.
     * 
     * @param \DateTime $dateTime
     */
    public function setLastNavigationTime(\DateTime $dateTime)
    {
       $this->session->lastNavigation = $dateTime;
    }
}
