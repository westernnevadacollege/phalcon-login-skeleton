<?php

use Phalcon\Mvc\Controller;

class LoginController extends Controller
{
    /**
     * Renders the index view for this controller
     */
    public function indexAction() {
        if (isset($this->session->auth)) {
            $this->flash->notice("It appears you're already logged in.");
        }
        
        // Whether to present CAPTCHA or not.
        $this->view->setVar('captcha', $this->persistent->get('captcha'));
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Log in the user.
     * 
     * Set's the 'auth' variable within the session. 
     * 
     * @param string $uid UID of person you'd like to log in.
     */
    protected function login($uid)
    {        
        $this->session->set('auth', array(
            'id' => $uid,
        ));
    }
        
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Attempts log in.
     * 
     * @return void
     */
    public function attemptAction()
    {
        if ($this->request->isPost()) {
            
            // If CAPTCHA was presented.         
            if (null != $this->request->getPost('recaptcha_challenge_field')) {
                // CAPTCHA validation only works once for each challenge presented. Thus 
                // we save the result.
                if (!($captchaValid = $this->captcha->isValid())) {
                    $this->flash->error($this->captcha->getError());
                    return $this->response->redirect(array('for' => 'login',));
                }
            }
            
            $uid = $this->request->getPost('uid');
            $password = $this->request->getPost('password');

            if ($this->userRepo->exists($uid)) {
                
                // Fetch user model.
                $user = Users::findFirst($uid);
                if ($user === false) {
                    $user = Users::createModel($uid);
                }
                
                $user->incrementLoginAttempt();
                                
                if ($user->getLoginAttempts() >= $this->config->general->maxLoginAttemptsCaptcha) {
                    // Start displaying captcha for subsequent requests.
                    $this->persistent->set('captcha', true);
                }
                
                if ($user->getLoginAttempts() >= $this->config->general->maxLoginAttemptsLock) {
                    $this->userRepo->lock($uid);
                }
                
                if ($user->getLoginAttempts() > $this->config->general->maxLoginAttemptsCaptcha) {
                    
                    // It's possible that a user didn't recieve a CAPTCHA challenge even after 
                    // they've gone passed the max allow attempts if the user is revisiting the
                    // application after they've already used their max attempts during another 
                    // visit. This is due to the fact that the CAPTCHA functionality depends on a 
                    // session and the session doesn't start until the first page load.
                    //
                    // The CAPTCHA challenge is to protected accounts from brute force attacks and 
                    // such.  If we don't require the CAPTCHA challenge during a first page 
                    // navigation, then it'd be pretty easy to route the attack to submit the form
                    // during that first page load in which CAPTCHA isn't required.
                    if ($captchaValid != true) {
                        $message = "Sorry, you must complete a CAPTCHA to log into this account. "
                                . "Please log in again.";
                        $this->flash->error($message);
                        return $this->response->redirect(array('for' => 'login',));
                    } 
                }
                
                if ($this->userRepo->authenticate($uid, $password) === true) {
                    
                    $this->login($uid);
                    $user->resetLoginAttempts();
                    return $this->response->redirect('home');
                    
                } else {
                    $this->flash->error('Invalid Credentials.');
                }
                
            } else {
                $this->flash->error('Invalid Credentials.');
            }
            
        }
        
        return $this->response->redirect(array('for' => 'login',));
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Log out action.
     * 
     * Restarts session and redirects to login.
     */
    public function logoutAction()
    {
        // Flash messages depend on a session. Given that, we need to store flash 
        // messages before we destory the session in order to render them within 
        // the next session.
        $stowedMessages = $this->flash->getMessages();
        
        // Restart the session, due to flash.
        $this->session->destroy();
        $this->session->start();
        
        // Restore the flash messages into the new session.
        foreach ($stowedMessages as $type => $messages) {
            foreach ($messages as $message) {
                $this->flash->message($type, $message);
            }
        }
        
        $this->response->redirect(array('for' => 'login',));
    }
}