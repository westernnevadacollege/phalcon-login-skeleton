<?php

/**
 *  Autoloader
 */

// Phalcon
$loader = new Phalcon\Loader();
$loader->registerDirs(array(
   '../platform/apps',
   '../platform/controllers',
   '../platform/common',
   '../platform/models',
))->register();

// composer
require_once root_path() . '/vendor/autoload.php';
