<?php

/**
 *  Create IoC
 */
$ioc = new Phalcon\DI\FactoryDefault(); // This pushes a variety

// -------------------------------------------------------------------------------------------------

/**
 *  Load configuration.
 */
$ioc->set('config', function() {
    return require platform_path() . '/_config/platform.php';
});

// -------------------------------------------------------------------------------------------------

/**
 * Dispatcher, dispatches routes to controllers.
 * 
 * Bootstrapped to include security.
 */
$ioc->set('dispatcher', function() use ($ioc) {
    
    // Fetch the events manager from the IoC container
    $eventsManager = $ioc->getShared('eventsManager');
    
    // Instantiate the Security object.
    $security = new Security($ioc);
    
    // Instantiate the Dispatcher object.
    $dispatcher = new \Phalcon\Mvc\Dispatcher;
    
    // Attach the event to the dispatcher object.
    $eventsManager->attach('dispatch', $security);
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
});

// -------------------------------------------------------------------------------------------------

/**
 * Database Service
 */

// Application's database
$ioc->set('db', function() use ($ioc) {
    
    $eventsManager = $ioc->getShared('eventsManager');
    
    $logger = new \Phalcon\Logger\Adapter\File(platform_path() . '/logs/debug.log');
    
    $eventsManager->attach('db', function($event, $connection) use ($logger) {
        if ($event->getType() == 'beforeQuery') {
            $logger->log($connection->getSQLStatement(), Phalcon\Logger::INFO);
        }
    });
    
    $mysql = new \Phalcon\Db\Adapter\Pdo\Mysql(array(
        'host' => $ioc->get('config')->database->host,
        'username' => $ioc->get('config')->database->username,
        'password' => $ioc->get('config')->database->password,
        'dbname' => $ioc->get('config')->database->dbname,
    ));
    
    $mysql->setEventsManager($eventsManager);
    
    return $mysql;
});

// Information System database
$ioc->set('isDb', function() use ($ioc){
    return new Phalcon\Db\Adapter\Pdo\Mysql(array(
        'host' => $ioc->get('config')->database->host,
        'username' => $ioc->get('config')->database->username,
        'password' => $ioc->get('config')->database->password,
        'dbname' => 'wncar_psis',
    ));
});

// -------------------------------------------------------------------------------------------------

/**
 * Router
 */
$ioc->set('router', function() {
    $router = require platform_path() . '/_config/routes.php';
    return $router;
});

// -------------------------------------------------------------------------------------------------

/**
 * Session
 */
$ioc->set('session', function() {
    $session = new \Phalcon\Session\Adapter\Files();
    $session->start();
    return $session;
});

// -------------------------------------------------------------------------------------------------

/**
 *  Setup a view component
 */
$ioc->set('view', function(){
    $view = new Phalcon\Mvc\View();
    $view->setViewsDir(platform_path() . '/views');
    return $view;
});

// -------------------------------------------------------------------------------------------------

/**
 * Flash
 */
$ioc->set('flash', function(){ 
    return new \Phalcon\Flash\Session();
});

// -------------------------------------------------------------------------------------------------

/**
 * Base URL
 */
$ioc->set('url', function() use ($ioc) {
    $url = new Phalcon\Mvc\Url;
    $url->setBaseUri($ioc->get('config')->general->baseurl);
    return $url;
});

// -------------------------------------------------------------------------------------------------

/**
 * User Repository
 */

$ioc->set('userRepo', function() use ($ioc) {
    $config = $ioc->get('config');
    
    $repo = new WesternNevadaCollege\Accounts\AccountService($config['repo.user']);
    
    return $repo->getRepo();
});

// -------------------------------------------------------------------------------------------------

/**
 * Person Biographical information
 */
$ioc->set('personBio', function() use ($ioc) {
    return new WesternNevadaCollege\PersonInfo\Biographical(array(
        'host' => $ioc->get('config')->database->host,
        'username' => $ioc->get('config')->database->username,
        'password' => $ioc->get('config')->database->password,
        'dbname' => 'wncar_psis',
        'driver' => 'mysql',
    ));
});

// -------------------------------------------------------------------------------------------------

/**
 * Google's reCaptcha service
 */
$ioc->setShared('captcha', function() use ($ioc) {
    $captcha = new ReCaptcha\Captcha();
    
    $captcha->setPrivateKey($ioc->get('config')->captcha->privateKey);
    $captcha->setPublicKey($ioc->get('config')->captcha->publicKey);
    
    return $captcha;
});


// -------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------
        // -----------------------------------------------------------------------------------------


return $ioc;
